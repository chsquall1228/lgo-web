<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
	Class to send push notifications using Google Cloud Messaging for Android
	Example usage
	-----------------------
	$an = new GCMPushMessage($apiKey);
	$an->setDevices($devices);
	$response = $an->send($message);
	-----------------------
	
	$apiKey Your GCM api key
	$devices An array or string of registered device tokens
	$message The mesasge you want to push out
*/
class GCMPushMessage {
	var $url = 'https://android.googleapis.com/gcm/send';
	var $serverApiKey = "";
	var $devices = array();
	var $CI;
	
	/*
		Constructor
		@param $apiKeyIn the server API key
	*/
	function __construct($apiKeyIn = null){
		$CI =& get_instance();
		$CI->load->config('gcm');
		if(!empty($apiKeyIn)){
			$this->serverApiKey = $apiKeyIn;
		}else{
			$this->serverApiKey = config_item("gcm_api_key");
		}
		$url =  config_item("gcm_server_url");
	}
	/*
		Set the devices to send to
		@param $deviceIds array of device tokens to send to
	*/
	function setDevices($deviceIds){
	
		if(is_array($deviceIds)){
			$this->devices = $deviceIds;
		} else {
			$this->devices = array($deviceIds);
		}
	
	}
	
	function addDevices($deviceIds){
		if(is_array($deviceIds)){
			foreach($deviceIds as $deviceId){
				array_push($this->devices, $deviceId);
			}
		} else {
			array_push($this->devices, $deviceIds);
		}
	}
	/*
		Send the message to the device
		@param $message The message to send
		@param $data Array of data to accompany the message
	*/
	function send($message, $data = false){
		
		if(!is_array($this->devices) || count($this->devices) == 0){
			$this->error("No devices set");
		}
		
		if(strlen($this->serverApiKey) < 8){
			$this->error("Server API Key not set");
		}
		
		$fields = array(
			'registration_ids'  => $this->devices,
			'data'              => array( "message" => $message ),
		);
		
		if(is_array($data)){
			foreach ($data as $key => $value) {
				$fields['data'][$key] = $value;
			}
		}
		$headers = array( 
			'Authorization: key=' . $this->serverApiKey,
			'Content-Type: application/json'
		);
		// Open connection
		$ch = curl_init();
		
		// Set the url, number of POST vars, POST data
		curl_setopt( $ch, CURLOPT_URL, $this->url );
		
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
		
		// Avoids problem with https certificate
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
		// Execute post
		$result = curl_exec($ch);
		// Close connection
		curl_close($ch);
		$json_result = json_decode($result, true);
		if(isset($json_result["failure"]) && $json_result["failure"] > 0){
			$error_msg = "";
			foreach($json_result["results"] AS $key=> $error){
				if(isset($error["error"])){
					$error_msg .= "[".$this->devices[$key]."] ".$error["error"]."\n";
				}
			}
			if(!empty($error_msg)){
				$this->error($error_msg);
			}
		}
		return $result;
	}
	
	function error($msg){
		throw new Exception($msg);
	}
}