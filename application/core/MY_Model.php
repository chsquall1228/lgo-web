<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model{
	/**
	 * Constructor
	 *
	 * @access public
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function select($attr ="*", $where=null, $group =null , $order=null, $limit = null, $from = null ){
			$sql = "SELECT ";
			if(!is_array($attr)){
				$sql .= $attr." ";
			}else{
				$last = count($attr)-1;
				foreach($attr as $key => $val){
					$sql .= " `$val` ";
					if($last > $key){
						$sql .= ",";
					}
				}
			}

			$sql .= " FROM `".$this->table."` ";

			if(!empty($where)){
				$sql .= " WHERE ";
				if(!is_array($where)){
					$sql .= $where." ";
				}else{
					$last = count($where)-1;
					$count = 0;
					foreach($where as $key =>$val){
						if(is_null($val)){
							$sql .= " `$key` = null ";
						}else{
							$val = $this->db->escape($val);
							$sql .= " `$key` = $val ";
						}
						if($last > $count){
							$sql .= "AND";
						}
						$count++ ;
					}
				}
			}

			if(!empty($group)){
				$sql .= " GROUP BY ";
				if(!is_array($group)){
					$sql .= $group." ";
				}else{
					$last = count($group)-1;
					foreach($group as $key => $val){
						$val = $this->db->escape($val);
						$sql .= " $val ";
						if($last > $key){
							$sql .= ",";
						}
					}
				}
			}

			if(!empty($order)){
				$sql .= " ORDER BY ";
				if(!is_array($order)){
					$sql .= $order." ";
				}else{
					$last = count($order)-1;
					foreach($order as $key =>$val){
						if(strpos($val,'-') === FALSE){
							$sql .= " `$val` ";
						}else{
							$sql .= " `".substr($val,1)."` desc ";
						}
						
						if($last > $key){
							$sql .= ",";
						}
					}
				}
			}

			if(!empty($limit)){
				$sql .=" LIMIT ".(!empty($from)?"$from, ":"").$this->db->escape($limit);
			}
			$result = $this->db->query($sql);
			$result = $this->result_in_array($result,$limit);
			return $result;
		}

		public function insert($value){
			$attr = array_keys($value);
			$sql = "INSERT into `".$this->table."` ";
			if(!empty($attr) && !is_numeric($attr[0])){
				$sql .= "( ";
				if(!is_array($attr)){
					$sql .= $attr." ";
				}else{
					$last = count($attr)-1;
					foreach($attr as $key => $val){
						$sql .= " `$val` ";
						if($last > $key){
							$sql .= ",";
						}
					}
				}
				$sql .= " ) ";
			}
			$sql .= " VALUES (";
			if(!is_array($value)){
				$sql .= $value." ";
			}else{
				$last = count($value)-1;
				$count = 0;
				foreach($value as $key => $val){
					if(is_null($val)){
						$sql .= " NULL ";
					}else{
						$val = $this->db->escape($val);
						$sql .= " $val ";
					}
					if($last > $count){
						$sql .= ",";
					}
					$count++;
				}
			}

			$sql .= " ); ";
			$result = $this->db->query($sql);
			return $result;
		}

		public function update($attr,$where=""){
			$sql = "Update `".$this->table."` SET ";
			if(!is_array($attr)){
				$sql .= $attr." ";
			}else{
				$last = count($attr)-1;
				$count = 0;
				foreach($attr as $key =>$val){
					if(is_null($val)){
						$sql .= " `$key` = null ";
					}else{
						$val = $this->db->escape($val);
						$sql .= " `$key` = $val ";
					}
					if($last > $count){
						$sql .= ",";
					}
					$count++;
				}
			}

			if(!empty($where)){
				$sql .= " WHERE ";
				if(!is_array($where)){
					$sql .= $where." ";
				}else{
					$last = count($where)-1;
					$count = 0;
					foreach($where as $key =>$val){
						if(is_null($val)){
							$sql .= " `$key` = null ";
						}else{
							$val = $this->db->escape($val);
							$sql .= " `$key` = $val ";
						}
						if($last > $count){
							$sql .= "AND";
						}
						$count++;
					}
				}
			}

			//echo $sql;
			$result = $this->db->query($sql);
			return $result;
		}

		public function delete($where=""){
			$sql = "DELETE FROM `".$this->table."` ";

			if(!empty($where)){
				$sql .= " WHERE ";
				if(!is_array($where)){
					$sql .= $where." ";
				}else{
					$last = count($where)-1;
					$count = 0;
					foreach($where as $key =>$val){
						$val = $this->db->escape($val);
						$sql .= " `$key` = $val ";
						if($last > $count){
							$sql .= "AND";
						}
						$count++;
					}
				}
			}

			$result = $this->db->query($sql);
			return $result;
		}

		public function result_in_array($result,$limit = null){
			$result_array = $result->result_array();
			if(!empty($limit) &&$limit == 1){
				return array_pop($result_array);
			}
			return $result_array;
		}

		public function last_insert(){
			return $this->db->insert_id();
		}

}

?>