<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class join_room_model extends MY_Model{

	/**
	 * Constructor
	 *
	 * @access public
	 */
	function __construct()
	{
		parent::__construct();
		$this->table = "join_room";
	}		
	
	function RetrieveGCMJoiner($filters=null){
		if(isset($filters["game_room_id"]) && !empty($filters["game_room_id"]) && !is_numeric($filters["game_room_id"])){
			return null;
		}
		
		if(isset($filteres["join_status"])){
			$ids = explode(",",$filters["join_status"]);
			foreach($ids as $id){
				if(!is_numeric($id)){
					return null;
				}
			}
		}
		
		
		if(isset($filters["user_permission"])){
			$ids = explode(",",$filters["user_permission"]);
			foreach($ids as $id){
				if(!is_numeric($id)){
					return null;
				}
			}
		}
		
		if(isset($filters["user_ids"])){
			$ids = explode(",",$filters["user_ids"]);
			foreach($ids as $id){
				if(!is_numeric($id)){
					return null;
				}
			}
		}
		
		$sql = "SELECT g.gcm_id
			FROM ".$this->table." j
			INNER JOIN user_gcm g ON j.user_id = g.user_id 
			WHERE 1=1 ";
			
		if(isset($filters["user_ids"])){
			$sql .= " AND j.user_id IN (".$filters["user_ids"].") ";
		}
		
		if(isset($filteres["join_status"])){
			$sql .= " AND j.join_status IN (".$filteres["join_status"].") ";
		}
		
		if(isset($filters["game_room_id"])){
			$sql .= " AND j.game_room_id = ".$filters["game_room_id"]." ";
		}
		
		if(isset($filters["user_permission"])){
			$sql .= " AND j.user_permission IN (".$filters["user_permission"].")";
		}
		
		if(isset($filters["avoid_sender"])){
			$sql .= " AND j.user_id != ".$filters["avoid_sender"]." ";
		}
		
		$result = $this->db->query($sql);	
		$result = $this->result_in_array($result);	
		return $result;
	}
	
}
?>