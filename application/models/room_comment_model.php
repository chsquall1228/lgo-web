<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class room_comment_model extends MY_Model{

	/**
	 * Constructor
	 *
	 * @access public
	 */

	function __construct()
	{
		parent::__construct();
		$this->table = "room_comment";
	}		

	function RetrieveComment($filter, $from = 0, $limit = 20){
		
		if(empty($from)){
			$from = 0;
		}elseif(!is_numeric($from)){
			return null;
		}
		
		if(empty($limit)){
			$limit = 20;
		}else if(!is_numeric($limit)){
			return null;
		}
		
		if(!empty($filter["game_room_id"]) && !is_numeric($filter["game_room_id"])){
			return null;
		}
		
		$sql = "SELECT c.*, COUNT(r.id) AS total_reply
			FROM ".$this->table." c
			LEFT JOIN ".$this->table." r ON r.parent_id = c.id 
			";
		$sql .= "
			WHERE c.game_room_id = ".$this->db->escape($filter["game_room_id"])." 
			AND ( c.parent_id IS NULL OR c.parent_id = 0)
			GROUP BY c.id 
			ORDER BY c.id DESC
			LIMIT $from, $limit ";
		$result = $this->db->query($sql);
		$result = $this->result_in_array($result,$limit);		
		return $result;	

	}
	
	function RetrieveReply($filter, $from = 0, $limit = 20){
		
		if(empty($from)){
			$from = 0;
		}elseif(!is_numeric($from)){
			return null;
		}
		
		if(empty($limit)){
			$limit = 20;
		}else if(!is_numeric($limit)){
			return null;
		}
		
		$sql = "SELECT c.*
			FROM ".$this->table." c
			WHERE c.game_room_id = ".$this->db->escape($filter["game_room_id"])." 
			AND c.parent_id = ".$this->db->escape($filter["parent_id"])."
			GROUP BY c.id 
			ORDER BY c.id DESC";
		
		$sql .= " LIMIT $from, $limit ";
		$result = $this->db->query($sql);		
		$result = $this->result_in_array($result,$limit);		
		return $result;	

	}
}
?>