<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

class Account extends MY_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model("user_model","user");
	}
	
	function VerifyEmail(){
		if(!empty($_GET["id"])){
			$where = array("id" => $_GET["id"], "status"=>0);
			$user = $this->user->select("id",$where, null, null, 1);
			if(!empty($user)){
				$this->load->helper('security');
				$salt = do_hash(time().mt_rand());
				$password = substr($salt, 0, 5);
				$update = array("status" => 1, "user_password" => $password);
				$result = $this->user->update($update, $where);
				if($result){
					echo "your temp password will be: ".$password;
				}else{
					echo "dun noob";
				}
			}else{
				echo "dun noob";
			}
		}else{
			echo "dun noob";
		}
	}
	
}