<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

class Report extends REST_Controller
{
	protected $methods = array(
		'crashreport_post' => array('key' => FALSE),
	);

	function __construct(){

		parent::__construct();

		$this->load->model("crash_report_model","crash_report");
	}

	
	function CrashReport_post(){
		$insert = array(
			"user_email" => $this->post("USER_EMAIL"),
			"phone_model" => $this->post("PHONE_MODEL"),
			"logcat" => $this->post("LOGCAT"),
			"android_version" =>  $this->post("ANDROID_VERSION"),
			"app_version_code" =>  $this->post("APP_VERSION_CODE"),
			"user_comment" =>  $this->post("USER_COMMENT"),
			"stack_trace" =>  $this->post("STACK_TRACE"),
			"report_time" =>  time()
		);
		$this->crash_report->insert($insert);
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= "From:admin@lgo.com" . "\r\n";
		$content = $this->report_build($this->post());
		//mail("chsquall1228@gmail.com,sean.william.chin@gmail.com","LGO Crash",$content, $headers);
		$this->response(null,200);
	}
	
	function report_build($json){
		$content = "<table border=1 cellspacing=0 >";
		foreach($json AS $key => $val){
			$content .= "<tr><td>$key</td><td>";
			if(!empty($val)){
				if(is_string($val) || is_numeric($val) || is_bool($val)){
					$content .= $val;
				}else{
					$content .= $this->report_build($val);
				}
			}
			$content .="</td></tr>";
		}
		$content .= "</table>";
		return $content;
	}

}