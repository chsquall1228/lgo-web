<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

class Status extends REST_Controller
{
	function __construct(){

		parent::__construct();
		$this->load->model("game_room_model","room");
		$this->load->model("join_room_model","join_room");
		$this->load->model("user_model","user");
		$this->load->model("room_comment_model","comment");
	}
	
	function GetLatest_get(){
		$existing_game_ids = $this->get("existing_game_ids");
		if(!empty($existing_game_ids)){
			$existing_game_ids = explode(",",$existing_game_ids);
		}
		$updated_time = $this->get("updated_time");
		$game_ids = array();
		$status = array("updated_time" => time());
		$status["games"] = $this->Games();
		foreach($status["games"] AS $game){
			array_push($game_ids, $game["id"]);
		}
		$status["comments"] = $this->Comments($game_ids);
		$status["joins"] = $this->Joins($game_ids);
		$user_ids = array();
		foreach($status["joins"] AS $join){
			array_push($user_ids, $join["user_id"]);
		}
		$user_ids = array_unique($user_ids);
		$status["users"] = $this->Users($user_ids);
		
		if(!empty($existing_game_ids)){
			$status["removed_games"] = array();
			foreach($existing_game_ids AS $existing_game_id){
				if(!in_array($existing_game_id, $game_ids)){
					$game = $this->room->select("*",array("id"=>$existing_game_id),null,null,1);
					$message = null;
					if($game["room_status"] > 0){
						$message = "the game has been removed";
					}else if($game["end_time"] < time()){
						$message = "the game is expired";
					}else{
						$joiner = $this->join_room->select("*",array("user_id"=>$this->rest->user_id, "game_id" => $existing_game_id),null,null,1);
						if($joiner["status"] > 0){
							$message = "the user has been leave";
						}
					}
					array_push($status["removed_games"], array("game_id" => $existing_game_id, "message"=>$message));
					
				}
			}
		}
		
		if(!empty($existing_game_ids) && !empty($updated_time)){
			foreach($status["games"] AS $key => $game){
				if(in_array( $game["id"],$existing_game_ids) && $game["updated_time"] <= $updated_time){
					unset($status["games"][$key]);
				}else{
					$status["games"][$key]["owner"] = array("id"=>$game["user_id"]);
				}
			}
			foreach($status["comments"] AS $key => $comment){
				if(in_array( $comment["game_room_id"],$existing_game_ids) && $comment["last_update_time"] <= $updated_time){
					unset($status["comments"][$key]);
				}else{
					$status["comments"][$key]["user"] = array("id"=>$comment["user_id"]);
				}
			}
			foreach($status["joins"] AS $key => $join){
				if(in_array( $join["game_room_id"],$existing_game_ids) && $join["join_time"] <= $updated_time){
					unset($status["joins"][$key]);
				}
			}
			$status["games"] = array_values($status["games"]);
			$status["comments"] = array_values($status["comments"]);
			$status["joins"] = array_values($status["joins"]);
		}
		return $this->response($status, 200);
	}
	
	function Games(){
		$filters["user_id"] = $this->rest->user_id;
		$filters["join_status"] = 0;
		$games = $this->room->retrieve_rooms($filters, null, 0, 999999, true);
		return $games;
	}
	
	function Users($user_ids){
		if(!empty($user_ids)){
			$select = array("id", "user_email", "user_name");
			$where = " `id` in (".implode(",",$user_ids).") ";
			return $this->user->select($select, $where);
		}else{
			return array();
		}
	}
	
	function Joins($game_ids){
		if(!empty($game_ids)){
			$where = " `game_room_id` in (".implode(",",$game_ids).") ";
			return $this->join_room->select("*", $where);
		}else{
			return array();
		}
	}
	
	function Comments($game_ids){
		if(!empty($game_ids)){
			$where = " `game_room_id` in (".implode(",",$game_ids).") ";
			$comments = $this->comment->select("*", $where);
			return $comments;
		}else{
			return array();
		}
	}


}