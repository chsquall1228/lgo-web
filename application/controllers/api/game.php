<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

class Game extends REST_Controller
{	

	protected $methods = array(		
		'game_get' => array('key' => FALSE),
		'games_get' => array('key' => FALSE),
		'joiner_get' => array('key' => FALSE),
		'comments_get' => array('key' => FALSE)
		);	
		
	function __construct(){
		parent::__construct();
		$this->load->model("game_room_model","room");
		$this->load->model("join_room_model","join_room");
		$this->load->model("user_model","user");
		$this->load->model("room_comment_model","comment");
		$this->load->model("user_gcm_model","user_gcm");
	}	
	
	function Location_get(){
		$user_id = $this->rest->user_id;
		$select = array(
			"location",
			"latitude",
			"longitude",
			"country"
		);
		
		$where = array(
			"user_id" => $user_id
		);
		
		$sorts = array(
			"updated_time"
		);
		$locations = $this->room->select($select, $where, null, $sorts);
		return $this->response($locations,200);
	}
	
	function Games_get(){
		$user_id = $this->rest->user_id;
		$user = $this->user->select(array("id","user_countries"),array("id"=>$user_id),null,null,1);
		$filters = array();
		$filters["game_category_ids"] = $this->get("game_category_ids");
		$filters["user_id"] = $user_id;
		$filters["search"] = $this->get("search");
		$filters["latitude"] = $this->get("latitude");
		$filters["longitude"]= $this->get("longitude");
		$filters["distance"] = $this->get("distance");
		$filters["countries"] = $this->get("countries");
		$filters["updated_time"] = $this->get("updated_time");
		$sorts = $this->get("sorts");
		if(!empty($sorts)){
			$sorts = explode(",", $sorts);
			if(!in_array("distance", $sorts)){
				array_push($sorts, "distance");
			}
		}else{
			$sorts = array("distance");
		}
		
		if(empty($filters["countries"])){
			$filters["countries"] = !empty($user_id)?$user["user_countries"]:null;
		}
		$limit = $this->get("limit");
		$from = $this->get("from");
		$games = $this->room->retrieve_rooms($filters, $sorts, $from, $limit);
		return $this->response($games,200);
	}
	
	function Game_get(){
		$where = array();
		if(empty($this->rest)){
			$where["private"] = 0;
		}
		$game_id = $this->get("id");
		$game = $this->room->select("*",array("id" => $game_id), null, null, 1);
		$game["joiners"] = $this->join_room->select("*",array("game_room_id" => $game_id));
		$game["owner"] = $this->user->select("*", array("id" => $game["user_id"]), null, null, 1);
		return $this->response($game,200);
	}		
	
	function Game_post(){		
		$game = null;		
		if( $this->post("id") == 0){			
			$insert = array(
				"name" => $this->post("name"),				
				"user_id" => $this->rest->user_id,
				"game_category_id" => $this->post("game_category_id"),
				"start_time" => $this->post("start_time"),	
				"end_time" => $this->post("end_time"),			
				"desc" => $this->post("desc"),			
				"location" => $this->post("location"),		
				"created_time" => time(),
				"longitude" => $this->post("longitude"),
				"latitude" => $this->post("latitude"),
				"private_room" => $this->post("private_room"),
				"country" => $this->post("country"),
				"updated_time" => time()
			);						
			$this->room->insert($insert);	
			$id = $this->room->last_insert();
			$where = array("id"=>$id);	
			$join_insert = array(
				"game_room_id" => $id,
				"user_id" => $this->rest->user_id,
				"user_permission" => 1,
				"join_status" => 0,
				"join_time" => time()
			);
			$this->join_room->insert($join_insert);
			$game = $this->room->select("*",$where,null,null,1);
		}else{		
			$update = array(		
				"name" => $this->post("name"),			
				"game_category_id" => $this->post("game_category_id"),	
				"start_time" => $this->post("start_time"),		
				"end_time" => $this->post("end_time"),			
				"desc" => $this->post("desc"),			
				"location" => $this->post("location"),
				"longitude" => $this->post("longitude"),
				"latitude" => $this->post("latitude"),
				"private_room" => $this->post("private_room"),
				"updated_time" => time(),
				"country" => $this->post("country")
			);			
			$where = array(			
				"id" => $this->post("id")	
				);			
			$this->room->update($update, $where);	
			$game = $this->room->select("*",$where,null,null,1);	
		}
		$this->SendNotification(array("avoid_sender" => $this->rest->user_id,"join_status" => 0, "game_room_id"=>$this->post("id")),$game["name"]." has updated");

		return $this->response($game,200);	
	}		
	
	function SendNotification($filters, $message){
		$joiners = $this->join_room->RetrieveGCMJoiner($filters);
		if(!empty($joiners)){
			$this->load->library("GCMPushMessage", null, "gcm");
			foreach($joiners AS $joiner ){
				$this->gcm->addDevices($joiner["gcm_id"]);
			}
			if(isset($filters["game_room_id"])){
				$this->gcm->send($message, array("game_id"=>$filters["game_room_id"]));
			}
		}
	}
	
	function Joiner_post(){
		return $this->joiner($this->post());
	}
	
	function Joiner_put(){
		return $this->joiner($this->put());
	}
	
	function joiner($join){
		$id = $join["id"];
		
		if(empty($id)){
			$joiner = $this->join_room->select("*", array("game_room_id" => $join["game_room_id"], "user_id" => $join["joiner"]["id"]), null, null, 1);
			if(!empty($joiner)){
				$id = $joiner["id"];
			}
		}
		
		if(empty($id)){
			$insert = array(
				"game_room_id" => $join["game_room_id"],
				"user_id" => $join["joiner"]["id"],
				"user_permission" => $join["user_permission"],
				"join_status" => $join["join_status"],
				"join_time"=> time()
			);
			$this->join_room->insert($insert);
		}else{
			$update = array(
				"user_permission" => $join["user_permission"],
				"join_status" => $join["join_status"],
				"join_time"=> time()
			);
			$this->join_room->update($update,array("id" => $id));
		}
		
		$game = $this->room->select("name",array("id"=>$join["game_room_id"]), null, null, 1);
		if($join_status == 2){
			$user = $this->user->select("user_name",array("id"=>$this->rest->user_id),null,null,1);
			$this->SendNotification(array("game_room_id" => $this->post("id"), "user_ids" => $join["joiner"]["id"]),$user["user_name"]." has invite you to join ".$game["name"]);
		}else if($join_status == 3){
			$user = $this->user->select("user_name",array("id"=>$join["joiner"]["id"]),null,null,1);
			$this->SendNotification(array("game_room_id" => $this->post("id"), "join_status" => 0, "user_permission" => "1,2"),$user["user_name"]." has requested to join ".$game["name"]);
		}else if($join_status == 0){
			$user = $this->user->select("user_name",array("id"=>$this->rest->user_id),null,null,1);
			$this->SendNotification(array("game_room_id" => $this->post("id"), "join_status" => 0, "user_permission" => "1,2"),$user["user_name"]." has joined ".$game["name"]);
		}else if($join_status == 1){
			$user = $this->user->select("user_name",array("id"=>$this->rest->user_id),null,null,1);
			$this->SendNotification(array("game_room_id" => $this->post("id"), "join_status" => 0, "user_permission" => "1,2"),$user["user_name"]." has leaved ".$game["name"]);
		}
		return $this->response(null,200);
	}
	
	function Joiner_get(){
		$id = $this->get("game_id");
		if(!empty($id)){
			$where = " `game_room_id` ='".$id."'";
			$joiners = $this->join_room->select("*", $where);
			if(!empty($joiners)){
				foreach($joiners AS &$join){
					$join["joiner"] = $this->user->select("*", array("id" => $join["user_id"]), null,null,1);
				}
				return $this->response($joiners,200);
			}
		}
		return $this->response(null,200);
	}
	
	function Game_delete(){	
		$where = array(			
			"id" => $_GET["id"]		
		);		
		$update = array(	
			"room_status" => 1		
			);		
		$this->room->update($update, $where);	
		return true;	
	}
	
	function Comments_get(){
		$where = array();
		$filter["game_room_id"] = $this->get("game_id");
		$filter["parent_id"] = $this->get("parent_id");
		$from = $this->get("from");
		$limit = $this->get("limit");
		$comments = null;
		if(empty($filter["parent_id"])){
			$comments = $this->comment->RetrieveComment($filter, $from , $limit);
		}else{
			$comments = $this->comment->RetrieveReply($filter, $from , $limit);
		}
		foreach($comments AS &$comment){
			$comment["user"] = $this->user->select("*",array("id" => $comment["user_id"]),null,null,1);
		}
		return $this->response($comments,200);
	}
	
	function Comment_get(){
		$where = array();
		$where["game_room_id"] = $this->get("game_id");
		$where["id"] = $this->get("id");
		$comment = $this->comment->select("*",$where, null, null, 1);
		$comment["user"] = $this->user->select("*",array("id" => $comment["user_id"]),null,null,1);
		return $this->response($comment,200);
	}
	
	function CommentReply_get(){
		$id = $this->get("id");
		$master = null;
		if(empty($from)){
			$from = 0;
		}
		
		if(empty($limit)){
			$limit = 20;
		}
		if(!empty($id)){
			$comments = $this->comment->select("*", array("parent_id" => $id), null, array("-id"), $from, $limit);
			foreach($comments AS &$comment){
				$comment["user"] = $this->user->select("*",array("id" => $comment["user_id"]),null,null,1);
			}
			return $this->response($comments,200);
		}
	}
	
	function Comment_post(){
		$insert = array(
			"comment" => $this->post("comment"),
			"game_room_id" => $this->post("game_room_id"),
			"last_update_time" => time(),
			"user_id" => $this->rest->user_id,
			"parent_id" => $this->post("parent_id")
		);
		if(empty($insert["parent_id"])){
			$insert["parent_id"] = null;
		}
		$this->comment->insert($insert);
		
		$game = $this->room->select("*",array("id"=>$this->post("game_room_id")), null, null, 1);
		$this->SendNotification(array("game_room_id"=>$this->post("game_room_id"), "join_status"=>0),$game["name"]." has new comments");
		
		return $this->response(null,200);
	}
	
	function Comment_put(){
		$update = array(
			"last_update_time" => time(),
			"comment" => $this->put("comment"),
		);
		$where = array(
			"id" => $this->put("id")
		);
		
		$this->comment->update($update, $where);
		
		$game = $this->room->select("*",array("id"=>$this->post("game_room_id")), null, null, 1);
		$this->SendNotification(array("game_room_id"=>$this->post("game_room_id"), "join_status" => 0),$game["name"]." has updated a comment");
		
		return $this->response(null,200);
	}
	
}