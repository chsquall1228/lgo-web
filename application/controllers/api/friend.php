<?php defined('BASEPATH') OR exit('No direct script access allowed');



/**

 * Example

 *

 * This is an example of a few basic user interaction methods you could use

 * all done with a hardcoded array.

 *

 * @package		CodeIgniter

 * @subpackage	Rest Server

 * @category	Controller

 * @author		Phil Sturgeon

 * @link		http://philsturgeon.co.uk/code/

*/
class Friend extends REST_Controller
{
	function __construct(){

		parent::__construct();

		$this->load->model("friend_model","friend");
		$this->load->model("user_model","user");
		$this->load->model("join_room_model","join_room");
		$this->load->model("user_gcm_model","user_gcm");
		$this->load->library('key');

	}

	

	function AddFren_post(){
		$friend = $this->post("friend");
		$fren_id = $friend["id"];
		if(empty($fren_id)){
			$user_fren = $this->user->select("id", array("id"=>$fren_id),  null, null, 1);
			if( empty($user_fren)){
				return $this->response(array("error" => "friend not found"), 500); 
			}
		}
		
		$existingFriend = $this->friend->select("user_id", array("user_id" => $this->rest->user_id, "fren_id"=>$fren_id),  null, null, 1);
		if(empty($existingFriend)){
			$insert = array(
				"user_id" => $this->rest->user_id,
				"fren_id" => $fren_id,
				"status" => 0,
				"permission" => 0,
				"updated_time" => time()
			);
			$this->friend->insert($insert);
			$insert = array(
				"user_id" => $fren_id,
				"fren_id" => $this->rest->user_id,
				"status" => 1,
				"permission" => 0,
				"updated_time" => time()
			);

			$this->friend->insert($insert);
		}else{
			$update = array(
				"status" => 0,
				"permission" => 0,
				"updated_time" => time()
			);
			$where = array(
				"user_id" => $this->rest->user_id,
				"fren_id" => $fren_id
			);
			$this->friend->update($update, $where);
			
			$update = array(
				"status" => 1,
				"permission" => 0,
				"updated_time" => time()
			);
			$where = array(
				"user_id" => $fren_id,
				"fren_id" => $this->rest->user_id
			);
			$this->friend->update($update, $where);
		}
		$user_gcm = $this->user_gcm->select(array("gcm_id"),array("user_id" => $fren_id));
		$this->load->library("GCMPushMessage", null, "gcm");
		foreach($user_gcm AS $gcm ){
			$this->gcm->addDevices($gcm["gcm_id"]);
		}
		$this->gcm->send($this->rest->user_name." has add you as friend", array("friend_id"=>$fren_id));
		return $this->response(null,200);

	}

	

	function Fren_put(){
		$friend = $this->put("friend");
		$fren_id = $friend["id"];
		$status = $this->put("status");
		$existingFriend = $this->friend->select("*", array("fren_id"=>$fren_id),  null, null, 1);
		if(empty($fren_id) || empty($existingFriend)){
			return $this->response(array("error" => "friend not found"), 500);
		}
		
		$update = array(
			"status" => $status
		);
			
		$this->friend->update($update, array("fren_id" => $fren_id, "user_id" => $this->rest->user_id));
		
		if($status != 3){
			$this->friend->update($update, array("fren_id" => $this->rest->user_id, "user_id" => $fren_id));
			
			$user_gcm = $this->user_gcm->select(array("gcm_id"),array("user_id" => $fren_id));
			$this->load->library("GCMPushMessage", null, "gcm");
			foreach($user_gcm AS $gcm ){
				$this->gcm->addDevices($gcm["gcm_id"]);
			}
			$this->gcm->send($this->rest->user_name." has accept your  friend request", array("friend_id"=>$fren_id));
		}
		return $this->response(null,200);
	}

	

	function Frens_get(){
		$where = null;
		if($this->get("operation") == "invite_room"){
			$where = "`game_room_id` = '".$this->get("game_id")."' AND `join_status` IN (0,2,3)";
			$joiners = $this->join_room->select("*", $where);
			$joiner_ids = array();
			if($joiners != null){
				foreach($joiners AS $joiner){
					array_push($joiner_ids, $joiner["user_id"]);
				}
			}
			$where = "user_id =".$this->rest->user_id;
			if(!empty($joiner_ids)){
				$where .= " AND fren_id NOT IN(".implode(",",$joiner_ids).")";
			}
		}else{
			$where = " `user_id` = ".$this->rest->user_id." && status != 3 && !((`status` = 0 OR `status` = 2) && (`updated_time` + 2592000) < ".time().") ";
		}
		
		$frens = $this->friend->select("*",$where);
		if(!empty($frens)){
			foreach($frens AS &$fren){
				$where = array(
					"id" => $fren["fren_id"]
				);
				$fren["friend"] = $this->user->select("*",$where, null, null, 1);
			}
		}
		
		return $this->response($frens,200);
	}
	
	function AvailableFren_get(){
		$search = $this->get("search");
		$user_id = $this->rest->user_id;
		$result = $this->user->get_search_user($search,$user_id);
		return $this->response($result, 200);
	}
}