<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

class Country extends REST_Controller
{	protected $methods = array(		
		'countries_get' => array('key' => FALSE)
		);	
		
	function __construct(){
		parent::__construct();
		$this->load->model("country_model","country");
	}	
	
	function Countries_get(){
		$countries = $this->country->select();
		return $this->response($countries,200);
	}
}
?>