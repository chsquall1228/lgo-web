<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

class Account extends REST_Controller
{
	protected $methods = array(
		'login_post' => array('key' => FALSE),
		'resetpassword_post' => array('key' => FALSE),
		'signup_post' => array('key' => FALSE),
	);
	
	function __construct(){
		parent::__construct();
		$this->load->model("user_model","user");		
		$this->load->model("user_invite_model","user_invite");		
		$this->load->model("join_room_model","join_room");
		$this->load->model("user_gcm_model","user_gcm");
		$this->load->library('key');
	}
	
	function Login_post(){
		$username = $this->post("user_email");
		$password = $this->post("user_password");
		$user = $this->user->get_user_login_detail($username,$password);
		if(empty($user)){
			return $this->response(array('error' => 'Invalid user name or password.'), 401);
		}else if(empty($user["key"])){
			return $this->response(array("key"=>$user["key"], "id"=>$user["id"], "user_name"=>$user["user_name"], "user_countries"=>$user["user_countries"], "user_email" => $user["user_email"] ), 200);
		}else{
			return $this->response(array("key"=>$user["key"], "id"=>$user["id"], "user_name"=>$user["user_name"], "user_countries"=>$user["user_countries"], "user_email" => $user["user_email"]),200);
		}
	}
	
	function SignUp_post(){
		$user_email = $this->post("user_email");
		$where = array("user_email" => $user_email);
		$user = $this->user->select(array("id", "status"), $where, null, null, 1);
		$send_email = false;
		$id = 0;
		if(empty($user)){
			$insert = array(
				"user_email" => $user_email,
				"status" => 0
			);
			$this->user->insert($insert);
			$id = $this->user->last_insert();
			$send_email = true;
		}else if($user["status"] == 0){
			$id = $user["id"];
			$send_email = true;
		}else{
			return $this->response(array('error' => 'Email Has Exist'));
		}
		
		if(!empty($id) && $send_email){
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= "From:admin@lgo.com" . "\r\n";						
			$title = "LGO email verify";			
			$content = "Click the link to verify your email. <a href='http://".$_SERVER['HTTP_HOST']."/master/account/VerifyEmail?id=".$id."'>Click me</a> Or ignore this email.";			
			if($_GET["operation"] == "invite"){				
				$invite = $this->user_invite->select("*", array("user_id"=>$this->rest->user_id, "invited_user_id"=>$id), null , null, 1);				
				if(!empty($invite)){					
					if(($invite["invite_time"] + 2592000) > time()){						
						return $this->response(array('error'=>'Invitation has been send this month'),500);					
					}else{						
						$this->user_invite->update(array("update_time" => time()),array("id" => $invite["id"]));					
					}				
				}else{					
					$insert =array(						
						"user_id" => $this->rest->user_id,						
						"invited_user_id" => $id,						
						"invite_time" => time()					
						);				
					$this->user_invite->insert($insert);				
				}				
				$user = $this->user->select("*",array("id"=>$this->rest->user_id),null,null,1);	
				$content = $user["user_name"]."(".$user["user_email"]."),<br/><br/>Has Invite you to join LGO. Please <a href='http://".$_SERVER['HTTP_HOST']."/master/account/VerifyEmail?id=".$id."'>Click me</a> to active your account or ignore this email";			
			}
			if(mail($user_email,$title,$content, $headers)){
				return $this->response(null,200);
			}else{
				return $this->response(array('error' => 'Unable to send out email'),500);
			}
		}
	}
	
	function ResetPassword_post(){
		$user_email = $this->post("user_email");
		if(!empty($user_email)){
			$where = array("user_email" => $user_email);
			$user = $this->user->select(array("id", "status"), $where, null, null, 1);
			if(empty($user)){
				return $this->response(array('error' => 'Email Not Exist'));
			}else{
				$this->load->helper('security');
				$salt = do_hash(time().mt_rand());
				$password = substr($salt, 0, 5);
				$update = array("status" => 1, "user_password" => $password);
				$where = array("id" => $user["id"]);
				$this->user->update($update, $where);
				if(mail($user_email,"LGO New Password","Here is your new password: $password")){
					return $this->response($user,200);
				}else{
					return $this->response(array('error' => 'Unable to send out email'),500);
				}
			}
		}else{
			return $this->response(array('error' => 'Are you kidding me'),500);
		}
	}
	
	function users_get(){		
		$where = array();		
		if($this->get("operation") == "invite_room"){			
			$joiners = $this->join_room->select("*", array("game_room_id" => $this->get("game_id")));			
			$joiner_ids = array($this->rest->user_id);			
			if($joiners != null){				
				foreach($joiners AS $joiner){					
					array_push($joiner_ids, $joiner["user_id"]);				
				}			
			}			
			$where = " id NOT IN(".implode($joiner_ids).") AND status = 1 AND ( user_email like '%".$this->get("search")."%' OR user_name like '%".$this->get("search")."%')";		
		}
		$from = $this->get("from");
		$limit = $this->get("limit");
		$users = $this->user->select("*",$where, null, null, $limit, $from);
		return $this->response($users, 200); 
	}
	
	function user_get(){
		$where = array(
			"id" => $this->get("id")
			);
		$user = $this->user->select("*",$where,  null, null, 1);
		return $this->response($user, 200); 
	}
	
	function RetrieveUserFromAPIKey_get(){
		$where = array(
			"id" => $this->rest->user_id
			);
		$user = $this->user->select("*",$where,  null, null, 1);
		return $this->response($user, 200); 
	}
	
	function user_put(){
		$where = array(
			"id" => $this->put("id")
		);
		$update = array(
			"user_name" => $this->put("user_name"),
			"user_phone" => $this->put("user_phone"),
			"user_countries" => $this->put("user_countries")
		);
		$this->user->update($update, $where);
		$user = $this->user->select("*",$where,  null, null, 1);
		return $this->response($user, 200); 
	}
	
	function ChangePassword_put(){
		$where = array(
			"id" => $this->put("id")
		);
		$user = $this->user->select("*",$where,  null, null, 1);
		if($user["user_password"] != $this->put("user_old_password")){
				return $this->response(array('error' => 'Invalid password'),401);
		}
		$update = array(
			"user_password" => $this->put("user_password")
		);
		$this->user->update($update, $where);
		$user = $this->user->select("*",$where,  null, null, 1);
		return $this->response($user, 200); 
	}
	
	function UserGcm_post(){
		$where = array(
			"user_id" => $this->rest->user_id
		);
		$gcms = $this->user_gcm->select("*", $where);
		if(empty($gcms)){
			foreach($gcms AS $gcm){
				if($gcm["gcm_id"] == $this->post("gcm_id")){
					return $this->response($gcm,200);
				}
			}
		}
		
		$insert = array(
			"user_id" => $this->rest->user_id,
			"gcm_id" =>	$this->post("gcm_id")
			);
		$this->user_gcm->insert($insert);
		return $this->response($insert, 200);
		
	}
}